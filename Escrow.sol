pragma solidity ^0.4.26;
    contract Escrow {
        address public buyer;
        address public seller;
        address public contract_addr;
        uint256 public amount;
        constructor(address _seller, address _contract_addr, uint256 _amount) public{
            buyer = msg.sender;
            seller = _seller;
            contract_addr = _contract_addr;
            amount= _amount;
        }
        function payToSeller() public{
            if(msg.sender == buyer) {
                ERC20(contract_addr).approve(msg.sender, amount);
                ERC20(contract_addr).transferFrom(msg.sender, seller, amount);
            }
        }
    }
            
    interface ERC20 {
        function new_token(uint256 value) external;
        function approve(address spender, uint256 amount) external returns (bool);
        function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    }