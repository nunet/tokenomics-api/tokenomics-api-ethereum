import sys
import grpc
import os
sys.path.append("./service_spec")

import tokenomics_pb2 as pb2
import tokenomics_pb2_grpc as pb2_grpc
import logging
import multiprocessing
import random
import time
if len(sys.argv) == 2:
    grpc_port = sys.argv[1]
else:
    grpc_port="9998"

def transaction(channel):
    time.sleep(random.random())
    pubk = "0xb5114121A51c6FfA04dBC73F26eDb7B6bfE2eB35"
    cost_per_process =  random.randint(10,100000)
    stub = pb2_grpc.TokenomicsStub(channel)
    escrow_address=stub.signTransaction(pb2.SignTransactionInput(cost_per_process=cost_per_process, pubk=pubk ))
    escrow_address=escrow_address.escrow_address
    txn=stub.makeTransaction(pb2.SignedResult(escrow_address=escrow_address))
    logging.info(txn)
    if txn.txn_hash!=None:
        logging.info("********result*******")
        logging.info(txn.txn_hash)
        count = len(open("result.txt").readlines())
        with open("result.txt","a+") as f:
            f.write(str(count)+"\n")
    return str(txn)

def result(channel):
    cost_per_process = 100000000
    stub = pb2_grpc.TokenomicsStub(channel)
    result="[{\n  \"agree\": 0.28646868,\n  \"disagree\": 0.25193137,\n  \"discuss\": 0.3719607,\n  \"unrelated\": 0.08963927\n}, {\n  \"time_taken\": 8,\n  \"network\": 50,\n  \"ram\": 100,\n  \"cpu\": 50\n}]"
    call_id="387"
    service_name="testing-uclnlp"
    txn_hash=stub.saveResult(pb2.SaveResultInput(result=result,call_id=call_id,service_name=service_name))
    response=stub.getResult(pb2.TxnHash(txn_hash=txn_hash.txn_hash))
    response=eval(response.response)
    return str(response)

def concurrent_test():
    n=10
    with open("result.txt","w") as f:
        f.write("result\n")
    with grpc.insecure_channel('localhost:'+str(grpc_port)) as channel:
        processes = [multiprocessing.Process(target=transaction,args=(channel,)) for x in range(int(n))]
        for p in processes:
            p.start()


with grpc.insecure_channel('localhost:'+str(grpc_port)) as channel:
    transaction(channel)

with grpc.insecure_channel('localhost:'+str(grpc_port)) as channel:
    result(channel)

concurrent_test()



