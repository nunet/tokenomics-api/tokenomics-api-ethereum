// SPDX-License-Identifier: MIT
pragma solidity ^0.5.0;
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/837828967a9831e4333d5fb9edefb200a357d24d/contracts/token/ERC20/ERC20.sol";
contract Token is ERC20 {
    address public owner;
    constructor () public ERC20() {
        owner=msg.sender;
    }
    
    function new_token(uint256 value) public {
        require(owner == msg.sender, "tokens only minted by contract owner");
        _mint(owner, value );
   }
}