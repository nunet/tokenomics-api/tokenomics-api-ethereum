from web3 import Web3
import json
import time
import configparser as cp
import os
class Result():
    def __init__(self):
        CONFIG = os.environ['CONFIG']
        self.config=cp.ConfigParser()
        self.config.read("config.ini")
        self.private_key=self.config[CONFIG].get("private_key")
        self.public_key=self.config[CONFIG].get("public_key")
        self.contract_address=self.config[CONFIG].get("result_contract_address")
        if "TEST" in CONFIG:
            self.infura_key=os.environ['rpc_address']
        else:
            self.infura_key=self.config[CONFIG].get("rpc_address")
        self.web3=Web3(Web3.HTTPProvider(self.infura_key))
        self.abi=json.load(open("abi-result.json", "r"))["abi"]
        self.contract=self.web3.eth.contract(address=Web3.toChecksumAddress(self.contract_address),abi=self.abi)

    def save_result(self,result,logger,ntx):
        nonce=ntx.nonce
        ntx.nonce+=1
        logger.info(nonce)
        txn=None
        while txn==None:
            try:
                tx = self.contract.functions.setResult(result).buildTransaction({
                    'gas': 500000,
                    'gasPrice': self.web3.toWei('50', 'gwei'),
                    'nonce': nonce
                })
                signed_tx=self.sign_transaction(tx)
                txn=self.transaction(signed_tx)
                txn=txn.transactionHash.hex()
            except Exception as e:
                if "replacement transaction underpriced" in str(e):
                    logger.info(e)
                    ntx.nonce=self.web3.eth.getTransactionCount(self.public_key,"pending")
                    nonce=ntx.nonce+1
        return txn

    def get_result(self,txn):
        txn=self.web3.eth.getTransaction(txn)
        txn_input=txn.input
        txn_input=txn_input[138:]
        txn_input=txn_input.rstrip("0")
        result=self.web3.toText(txn_input)
        return result
    
    def sign_transaction(self, tx):
        signed_tx=self.web3.eth.account.signTransaction(tx,self.private_key)
        return signed_tx
    
    def transaction(self,signed_tx):
        tx_hash=self.web3.eth.sendRawTransaction(signed_tx.rawTransaction)
        txn=None
        while txn==None:
            try:
                txn=self.web3.eth.getTransactionReceipt(tx_hash)
            except:
                pass
        return txn        
