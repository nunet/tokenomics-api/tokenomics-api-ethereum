from web3 import Web3
import json
import time
import configparser as cp
import os
import eth_account
import sys
sys.path.append("./service_spec")
import tokenomics_pb2
import tokenomics_pb2_grpc

import stats_db_pb2 as pb2
import stats_db_pb2_grpc as pb2_grpc

class ntx():
    def __init__(self):
        CONFIG = os.environ['CONFIG']
        self.config=cp.ConfigParser()
        self.config.read("config.ini")
        self.private_key=self.config[CONFIG].get("private_key")
        self.public_key=self.config[CONFIG].get("public_key")
        self.contract_address=self.config[CONFIG].get("contract_address")
        if "TEST" in CONFIG:
            self.infura_key=os.environ['rpc_address']
        else:
            self.infura_key=self.config[CONFIG].get("rpc_address")
        self.web3=Web3(Web3.HTTPProvider(self.infura_key))
        self.abi=json.load(open("abi.json", "r"))["abi"]
        self.contract=self.web3.eth.contract(address=Web3.toChecksumAddress(self.contract_address),abi=self.abi)

        self.escrow_bytecode = json.load(open("escrow_abi.json", "r"))["bytecode"]
        self.escrow_abi = json.load(open("escrow_abi.json", "r"))["abi"]
        self.nonce=self.web3.eth.getTransactionCount(self.public_key,"pending")
    
    def mine(self,value,logger):
        nonce=self.nonce
        self.nonce+=1
        logger.info(nonce)            
        tx = self.contract.functions.new_token(value).buildTransaction({
                    'gas': 500000,
                    'gasPrice': self.web3.toWei('50', 'gwei'),
                    'nonce': nonce,
                })
        
        signed_tx=self.web3.eth.account.signTransaction(tx,self.private_key)
        try:
            self.transaction(signed_tx)
        except:
            pass

    def increase_allowance(self,value,public_key,logger):
        nonce=self.nonce
        self.nonce+=1
        logger.info(nonce)
        txn=None

        while txn==None:
            try:
                tx = self.contract.functions.increaseAllowance(public_key,int(value)).buildTransaction({
                    'gas': 500000,
                    'gasPrice': self.web3.toWei('50', 'gwei'),
                    'nonce': nonce,
                })
                signed_tx=self.web3.eth.account.signTransaction(tx,self.private_key)
 
                txn=self.transaction(signed_tx)
            except Exception as e:
                logger.error(e)
                if "replacement transaction underpriced" in str(e):
                    logger.info(e)
                    self.nonce=self.web3.eth.getTransactionCount(self.public_key,"pending")
                    nonce=self.nonce+1
        return txn

    def get_balance(self,public_key):
        balance = self.contract.functions.balanceOf(public_key).call()
        return balance
    
    def transaction(self,signed_tx, mining=False):
        txn=None
        tx_hash=self.web3.eth.sendRawTransaction(signed_tx.rawTransaction)
        if mining:
            return "mined"
        while txn==None:
            try:
                txn=self.web3.eth.getTransactionReceipt(tx_hash)
            except:
                pass
        return txn        

    def release_funds(self,escrow_address,logger,channel,escrow_info):
        contract=self.web3.eth.contract(address=Web3.toChecksumAddress(escrow_address),abi=self.escrow_abi)
        nonce=self.nonce
        self.nonce+=1
        logger.info(nonce)
        txn=None
        while txn==None:
            try:
                tx = contract.functions.payToSeller().buildTransaction({
                    'gas': 500000,
                    'gasPrice': self.web3.toWei('50', 'gwei'),
                    'nonce': nonce,
                })
                signed_tx=self.web3.eth.account.signTransaction(tx,self.private_key)
 
                txn=self.transaction(signed_tx)
            except Exception as e:
                if "replacement transaction underpriced" in str(e):
                    logger.info(e)
                    self.nonce=self.web3.eth.getTransactionCount(self.public_key,"pending")
                    nonce=self.nonce+1
        stub = pb2_grpc.StatsDatabaseStub(channel)
        stub.db_update_ntx(pb2.DatabaseNtxPaidInput(escrow_address=str(escrow_info)))
        return txn

    def get_sign(self, signed_tx):
        signed_tx=json.loads(signed_tx)        
        signed_tx=eth_account.datastructures.SignedTransaction(signed_tx["rawTransaction"],
                        signed_tx["hash"],signed_tx["r"],signed_tx["s"],signed_tx["v"])
        return signed_tx 


    def deploy(self,seller,amount,logger):
        buyer=self.public_key
        contract_address=self.contract_address

        Escrow = self.web3.eth.contract(abi=self.escrow_abi, bytecode=self.escrow_bytecode)
        seller=self.web3.toChecksumAddress(str(seller))
        logger.info(seller)
        nonce=self.nonce
        self.nonce+=1
        logger.info(nonce)
        txn=None
        while txn==None:
            try:
                tx=Escrow.constructor(seller,contract_address,amount).buildTransaction({
                    'gas': 500000,
                    'gasPrice': self.web3.toWei('50', 'gwei'),
                    'nonce': nonce,
                    })
                signed_tx=self.web3.eth.account.signTransaction(tx,self.private_key)
 
                txn=self.transaction(signed_tx)
            except Exception as e:
                if "replacement transaction underpriced" in str(e):
                    logger.info(e)
                    nonce=self.nonce+1
        return txn.contractAddress


