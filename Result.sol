pragma solidity ^0.6.8;

contract Result {
   string private result;
   
   function setResult(string memory _result) public { result = _result; }
   function getResult() public view returns(string memory) { return result; }
}
