#!/usr/bin/env python3

from concurrent import futures
import sys
import grpc
import time
import ast

sys.path.append("./service_spec")
import tokenomics_pb2
import tokenomics_pb2_grpc

import stats_db_pb2 as pb2
import stats_db_pb2_grpc as pb2_grpc

from ntx import *
from result import *

import log
import time
import subprocess
import os

from time import sleep

import opentelemetry
from opentelemetry import trace
from opentelemetry.instrumentation.grpc import GrpcInstrumentorServer
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.exporter.jaeger.thrift import JaegerExporter
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace import set_span_in_context

import threading

jaeger_address = os.environ['jaeger_address']

stats_db_address = os.environ['stats_db_address']


jaeger_exporter = JaegerExporter(
    agent_host_name=jaeger_address,
    agent_port=6831,
)


trace_provider=trace.get_tracer_provider().add_span_processor(
    BatchSpanProcessor(jaeger_exporter)
)

grpc_server_instrumentor = GrpcInstrumentorServer()
grpc_server_instrumentor.instrument()

tracer_server=opentelemetry.instrumentation.grpc.server_interceptor(tracer_provider=trace_provider)
tracer=trace.get_tracer(__name__)

ntx=ntx()
resultUtils=Result()

logger = log.setup_custom_logger('TRACE')

if len(sys.argv) == 2:
    grpc_port = sys.argv[1]
else:
    grpc_port="9998"
class Tokenomics(tokenomics_pb2_grpc.TokenomicsServicer):
    def signTransaction(self, request, context):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        sleep(30 / 1000)
        cost_per_process=request.cost_per_process
        pubk=request.pubk
        call_id=request.call_id

        logger.info("public key: "+pubk)
        logger.info("value: "+str(cost_per_process))   
        logger.info("call id: "+str(call_id))   
   
        #ntx.mine(cost_per_process,logger)  
        ntx_mine = threading.Thread(target=ntx.mine, args=(cost_per_process,logger,))
        ntx_mine.start()
        escrow_address=ntx.deploy(pubk,cost_per_process,logger)
        
        service_escrow_address=eval(call_id)
        service_escrow_address["escrow_address"]=str(escrow_address)
        call_id=eval(call_id)["call_id"]
        
        
        
        current_span.add_event("event message", {"escrow address": str(escrow_address)})
        current_span.add_event("event message", {"service escrow address": str(service_escrow_address)})

        channel = grpc.insecure_channel("{}".format(stats_db_address))
        stub = pb2_grpc.StatsDatabaseStub(channel)
        result=stub.update_call(pb2.DatabaseUpdateCallInput(call_id=str(call_id), escrow_address=str(service_escrow_address)))

        txn=ntx.increase_allowance(cost_per_process,escrow_address,logger)
        #ntx_increase_allowance= threading.Thread(target=ntx.increase_allowance, args=(cost_per_process,escrow_address,logger,))
        #ntx_increase_allowance.start()
        
        return tokenomics_pb2.SignedResult(escrow_address=escrow_address)
    
    def makeTransaction(self, request, context):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        sleep(30 / 1000)
        escrow_address=request.escrow_address
        escrow_info=""
        channel = grpc.insecure_channel("{}".format(stats_db_address))
        release_fund = threading.Thread(target=ntx.release_funds, args=(escrow_address,logger,channel,escrow_info,))
        release_fund.start()
        txn="fund released"
        current_span.add_event("event message", {"release fund transaction hash": str(txn)})
        return tokenomics_pb2.TxnHash(txn_hash=str(txn))

    def saveResult(self, request, context):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        sleep(30 / 1000)
        result=request.result
        call_id=request.call_id
        service_name=request.service_name
        logger.info("result: "+result)
        logger.info("call id: "+call_id)
        txn=resultUtils.save_result(result,logger,ntx)
        logger.info("tx_hash: "+txn)
        current_span.add_event("event message", {"save result transaction hash": str(txn)})
        current_span.add_event("event message", {"call id": str(call_id)})
        current_span.add_event("event message", {"service name": str(service_name)})
        
        res= {"service_name":str(service_name),
                "txn_hash":str(txn)
                    }
        
        with grpc.insecure_channel("{}".format(stats_db_address)) as channel:
            stub = pb2_grpc.StatsDatabaseStub(channel)
            stub.add_txn(pb2.DatabaseAddTransactionInput(call_id=str(call_id),txn_hash=str(res)))

            stub = pb2_grpc.StatsDatabaseStub(channel)
            result=stub.add_result(pb2.DatabaseAddResultInput(call_id=str(call_id),result=str(result)))        
        
            escrow_addresses=[]
            while len(escrow_addresses)<3:
                channel = grpc.insecure_channel("{}".format(stats_db_address))
                stub = pb2_grpc.StatsDatabaseStub(channel)
                result=stub.get_escrow(pb2.DatabaseGetEscrowInput(call_id=str(call_id)))
                escrow_addresses=result.escrow_address
                escrow_addresses=eval(escrow_addresses)
                time.sleep(60)

        logger.info("escrow addresses: "+str(escrow_addresses))
        escrow_info=""
        for item in escrow_addresses:
            logger.info(str(service_name))
            logger.info(str(item))
            if eval(eval(item)["escrow_address"])["service"] in service_name:
                escrow_info=eval(item)["escrow_address"]
                escrow_address=eval(eval(item)["escrow_address"])["escrow_address"]
                break
        logger.info("escrow address: "+escrow_address)
        current_span.add_event("event message", {"escrow addresses": str(escrow_addresses)})
        channel = grpc.insecure_channel("{}".format(stats_db_address))
        release_fund = threading.Thread(target=ntx.release_funds, args=(escrow_address,logger,channel,escrow_info,))
        release_fund.start()
        stub = pb2_grpc.StatsDatabaseStub(channel)
        stub.db_update_ntx(pb2.DatabaseNtxPaidInput(escrow_address=str(escrow_info)))
        return tokenomics_pb2.TxnHash(txn_hash=str(txn))


    def getResult(self, request, context):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        sleep(30 / 1000)
        txn_hash=request.txn_hash
        logger.info("tx_hash: "+txn_hash)
        response=resultUtils.get_result(txn_hash)
        current_span.add_event("event message", {"result from blockchain": str(response)})
        logger.info("response: "+response)
        return tokenomics_pb2.GetResultOutput(response=str(response))

def main():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1000))
    tokenomics_pb2_grpc.add_TokenomicsServicer_to_server(Tokenomics(), server)
    server.add_insecure_port('[::]:'+str(grpc_port))
    server.start()
    logger.info("Server listening on 0.0.0.0:{}".format(grpc_port))
    try:
        while True:
            time.sleep(86400)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    main()

